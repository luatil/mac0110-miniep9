using Test
using LinearAlgebra

function checkMatrixDiff(a, b)  
    episilon = 0.00001
    dim = size(a)
    for i in 1:dim[1]
        for j in 1:dim[2]
            if abs(a[i, j] - b[i, j]) > episilon * a[i, j]
                return false
            end
        end
    end
    return true
end

function test_matrix_pot()
    @test checkMatrixDiff(matrix_pot([1 2; 3 4], 1), [1 2; 3 4])
    @test checkMatrixDiff(matrix_pot([1 2 ; 3 4], 2) , [7.0 10.0; 15.0 22.0])
    @test checkMatrixDiff(matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7) , [5.64284e8 4.70889e8 3.23583e8 3.51859e8; 8.31242e8 6.93529e8 4.76619e8 5.18192e8; 5.77004e8 4.81473e8 3.30793e8 3.59677e8; 7.99037e8 6.66708e8 4.58121e8 4.98127e8])
    println("The function matrix_pot is working")
end

function test_matrix_pot_by_squaring()
    @test checkMatrixDiff(matrix_pot_by_squaring([1 2; 3 4], 1), [1 2; 3 4])
    @test checkMatrixDiff(matrix_pot_by_squaring([1 2 ; 3 4], 2) , [7.0 10.0; 15.0 22.0])
    @test checkMatrixDiff(matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7) , [5.64284e8 4.70889e8 3.23583e8 3.51859e8; 8.31242e8 6.93529e8 4.76619e8 5.18192e8; 5.77004e8 4.81473e8 3.30793e8 3.59677e8; 7.99037e8 6.66708e8 4.58121e8 4.98127e8])
    @test checkMatrixDiff(matrix_pot_by_squaring([2 0; 0 2], 10),  [1024.0 0; 0 1024.0] )
    println("The function matrix_pot_by_squaring is working")
end

    
function multiplica(a, b)
    # this creates a tuple with the size of a 
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
        return -1
    end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    return c
end

function matrix_pot(M, p)
    ans = M
    while p > 1
        ans = multiplica(ans, M)
        p -= 1
    end
    return ans
end

function number_to_binary_arrary(n)
    arr = []
    while n > 0
        temp = n % 2
        push!(arr, temp)
        n = div(n, 2)
    end
    return arr
end

function matrix_to_square_power(M, s) 
    while s > 0
        M = multiplica(M, M)
        s -= 1
    end
    return M
end

function identity_matrix(M)
    dim = size(M)
    ans = zeros(dim[1], dim[2])
    for i in 1:dim[1]
        for j in 1:dim[2]
            if i == j
                ans[i, j] = 1
            end
        end
    end
    return ans
end

function matrix_pot_by_squaring(M, p) 
    arr = number_to_binary_arrary(p)
    base = M 
    total = identity_matrix(M)
    for i in 1:length(arr)
        if arr[i] == 1
            total = multiplica(total, matrix_to_square_power(base, i-1))
        end
    end
    return total 
end

function compare_times()
    M = Matrix(LinearAlgebra.I, 30, 30)
    println("Matrix 30x30 to 10th power")
    println("Normal:")
    @time matrix_pot(M, 10)
    println("Squaring:")
    @time matrix_pot_by_squaring(M, 10)

    println("Matrix 2x2 to 2000th power")
    println("Normal:")
    @time matrix_pot([1 2; 3 4], 2000)
    println("Squaring:")
    @time matrix_pot_by_squaring([1 2; 3 4], 20)

    println("Matrix 4x4 to the 7th power")
    println("Normal:")
    @time matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7)
    println("Squaring:")
    @time matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7)
end
    